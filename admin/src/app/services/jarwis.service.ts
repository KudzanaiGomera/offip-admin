import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JarwisService {
  get_all_user() {
    throw new Error('Method not implemented.');
  }

  private baseUrl = 'https://offip.co.za/Backend/api';

  constructor(
    private http: HttpClient,
  ) { }

  loggedIn() {
    return localStorage.getItem('token');
  }

  login(data: any) {
    return this.http.post(`${this.baseUrl}/adminLogin.php`, data)
  }

  getUsers() {
    return this.http.get(`${this.baseUrl}/listUsers.php`)
  }

  getSubadmins() {
    return this.http.get(`${this.baseUrl}/listSubadmin.php`)
  }

  addUser(data: any) {
    return this.http.post(`${this.baseUrl}/addUser.php`, data)
  }

  addSubadmin(data: any) {
    return this.http.post(`${this.baseUrl}/addSubadmin.php`, data)
  }

  resetPassword(data: any): Observable<HttpEvent<any>> {
    const req = new HttpRequest('POST', `${this.baseUrl}/resetPassword.php`, data, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.request(req);
  }

  resetPassword2(data: any): Observable<HttpEvent<any>> {
    const req = new HttpRequest('POST', `${this.baseUrl}/resetPassword2.php`, data, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.request(req);
  }

  disableUser(data: any) {
    return this.http.post(`${this.baseUrl}/disableUser.php`, data)
  }


  activateUser(data: any) {
    return this.http.post(`${this.baseUrl}/activateUser.php`, data)
  }

  deleteUser(data: any) {
    return this.http.post(`${this.baseUrl}/deleteUser.php`, data)
  }

  deleteSubadmin(data: any) {
    return this.http.post(`${this.baseUrl}/deleteSubadmin.php`, data)
  }

  createFooter(data: any): Observable<HttpEvent<any>> {
    const req = new HttpRequest('POST', `${this.baseUrl}/createLooter.php`, data, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.request(req);
  }

  createLogo(data: any): Observable<HttpEvent<any>> {
    const req = new HttpRequest('POST', `${this.baseUrl}/createLogo.php`, data, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.request(req);
  }

  getFooter() {
    return this.http.get(`${this.baseUrl}/getLooter.php`)
  }

  getLogo() {
    return this.http.get(`${this.baseUrl}/getLogo.php`)
  }

  getSurvey() {
    return this.http.get(`${this.baseUrl}/getSurvey.php`);
  }

  getSurveys() {
    return this.http.get(`${this.baseUrl}/getSurveys.php`);
  }

  getSurveyReport(data: any) {
    return this.http.post(`${this.baseUrl}/getSurveyReport.php`, data);
  }

  getSurveyBySurvey(data: any) {
    return this.http.post(`${this.baseUrl}/getSurveyBySurvey.php`, data);
  }

  getAccount(data: any) {
    return this.http.post(`${this.baseUrl}/getAccount.php`, data);
  }

  getAdminAccount(data: any) {
    return this.http.post(`${this.baseUrl}/getAdminAccount.php`, data);
  }

  adminAccount(data: any) {
    return this.http.post(`${this.baseUrl}/adminAccount.php`, data);
  }

  deleteSurvey(data: any) {
    return this.http.post(`${this.baseUrl}/deleteSurvey.php`, data)
  }

  upload(data: any): Observable<HttpEvent<any>> {
    const req = new HttpRequest('POST', `${this.baseUrl}/adminUpload.php`, data, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.request(req);
  }

  getImage(data: any) {
    return this.http.post(`${this.baseUrl}/getAdminImage.php`, data)
  }

  getAllReports() {
    return this.http.get(`${this.baseUrl}/getAllReports.php`)
  }

  uploadReport(data: any): Observable<HttpEvent<any>> {
    const req = new HttpRequest('POST', `${this.baseUrl}/uploadReport.php`, data, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.request(req);
  }

  getReports(data: any) {
    return this.http.post(`${this.baseUrl}/getReports.php`, data)
  }

  deleteReport(data: any) {
    return this.http.post(`${this.baseUrl}/deleteReport.php`, data)
  }

}
