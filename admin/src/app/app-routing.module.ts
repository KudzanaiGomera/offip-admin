import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { AccountComponent } from './pages/account/account.component';
import { HomeComponent } from './pages/home/home.component';
import { ReportDataComponent } from './pages/report-data/report-data.component';
import { ReportModalComponent } from './pages/report-modal/report-modal.component';
import { SubadminComponent } from './pages/subadmin/subadmin.component';
import { UploadComponent } from './pages/upload/upload.component';
import { UserManagementComponent } from './pages/user-management/user-management.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },

  {
    path: 'login',
    component: LoginComponent,
  },

  {
    path: 'home',
    component: HomeComponent,
  },

  {
    path: 'usermanagement',
    component: UserManagementComponent,
  },

  {
    path: 'subadmin',
    component: SubadminComponent,
  },

  {
    path: 'report',
    component: ReportDataComponent,
  },

  {
    path: 'report/:name',
    component: ReportModalComponent,
  },

  {
    path: 'account',
    component: AccountComponent,
  },

  {
    path: 'upload',
    component: UploadComponent,
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
