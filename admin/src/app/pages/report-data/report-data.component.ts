import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { JarwisService } from 'src/app/services/jarwis.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-report-data',
  templateUrl: './report-data.component.html',
  styleUrls: ['./report-data.component.css']
})
export class ReportDataComponent implements OnInit {


  public loggedIn: boolean = false;
  public token: any;
  public users: any;
  public user: any;
  public surveys: any;
  public error = null;
  public success = null;
  public pic: any;

  constructor(
    private Jarwis: JarwisService,
    private router: Router,
    private Token: TokenService,
    private Auth: AuthService,
  ) { this.router.routeReuseStrategy.shouldReuseRoute = () => false; }

  openSurvey(name: string) {
    this.router.navigate(['/report', name]);
  }

  deleteSurvey(survey: any) {
    this.Jarwis.deleteSurvey({ 'survey': survey }).subscribe((data) =>
      this.handleResponse(data)
    );
  }

  handleResponse(data: any) {
    this.ngOnInit();
  }

  handleSuccess(success: any) {
    this.success = success.message;
  }

  handleError(error: any) {
    this.error = error.error.error;
  }

  ngOnInit() {

    this.Auth.authStatus.subscribe((value: boolean) => this.loggedIn = value);
    this.token = localStorage.getItem('token');
    this.user = this.Token.payload(this.token);
    this.user = this.user.data.name;

    this.Jarwis.getSurveys().subscribe((response: any) => {
      this.surveys = response['surveys'];
    })

    //get admin profile image
    let myFormData = new FormData();
    myFormData.append('name', this.user);

    this.Jarwis.getImage(myFormData).subscribe((response: any) => {
      this.pic = response['Image'];
      console.log(this.pic);
    },
      error => this.handleError(error),
    );

  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    localStorage.clear();
    this.router.navigate(['']);
  }

}
