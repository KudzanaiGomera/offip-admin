import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { JarwisService } from 'src/app/services/jarwis.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public loggedIn: boolean = false;
  public token: any;
  public users: any;
  public surveys: any;
  public user: any;
  public numberOfUsers: any;
  public numberOfSurveys: any;
  public pic: any;
  public error: any;
  public success: any;
  public reports: any;
  public reportsNo: any;

  constructor(
    private Jarwis: JarwisService,
    private Auth: AuthService,
    private Token: TokenService,
    private router: Router
  ) { }

  goToAccount() {
    this.router.navigateByUrl('/account');
  }

  ngOnInit() {

    this.Auth.authStatus.subscribe((value: boolean) => this.loggedIn = value);
    this.token = localStorage.getItem('token');
    this.user = this.Token.payload(this.token);
    this.user = this.user.data.name;

    this.Jarwis.getUsers().subscribe((response: any) => {
      this.users = response['users'];
      //get the number of objects which is one user per object
      this.numberOfUsers = this.users.length;
    });

    this.Jarwis.getSurveys().subscribe((response: any) => {
      this.surveys = response['surveys'];
      //get the length of the array which is the number of Surveys
      this.numberOfSurveys = this.surveys.length;
    });

    //get admin profile image
    let myFormData = new FormData();
    myFormData.append('name', this.user);

    this.Jarwis.getImage(myFormData).subscribe((response: any) => {
      this.pic = response['Image'];
    },
      error => this.handleError(error),
    );

    this.Jarwis.getAllReports().subscribe((response: any) => {
      this.reports = response['reports'];
      this.reportsNo = this.reports.length;
    },
      error => this.handleError(error),
    );

  }

  handleError(error: any) {
    this.error = error.error.error;
  }

  handleSuccess(success: any) {
    this.success = success.message;
  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    localStorage.clear();
    this.router.navigate(['']);
  }

}
