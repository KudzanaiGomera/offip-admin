import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JarwisService } from 'src/app/services/jarwis.service';
import { TokenService } from 'src/app/services/token.service';
import { HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  public loggedIn: boolean = false;
  public error: any;
  public success = null;
  public token: any;
  public admins: any;
  public username = '';
  public selectedFile: any = null;
  public profile: any;
  public pic: any | undefined;
  public surveys: any;
  public reports: any;
  progress = 0;

  public form = {
    survey: ''
  }

  constructor(
    private Jarwis: JarwisService,
    private router: Router,
    private Token: TokenService,
  ) { this.router.routeReuseStrategy.shouldReuseRoute = () => false; }

  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
  }

  handleResponse(data: any) {
    this.ngOnInit();
  }

  deleteReport(survey: any) {
    this.Jarwis.deleteReport({ 'survey': survey }).subscribe((data) =>
      this.handleResponse(data)
    );
  }

  onSubmit() {

    this.token = localStorage.getItem('token');
    this.admins = this.Token.payload(this.token);
    this.username = this.admins.data.name;

    this.progress = 0;

    if (this.selectedFile === null) {
      this.error = "Please select an image.";
    }
    else {
      let myFormData = new FormData();
      myFormData.append('file', this.selectedFile, this.selectedFile.name);
      myFormData.append('survey', this.form.survey);
      myFormData.append('subadmin', this.username);

      this.Jarwis.uploadReport(myFormData).subscribe(
        (event: any) => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progress = Math.round(100 * event.loaded / event.total);
          }
        },
        error => this.handleError(error),
      );
    }
  }

  handleSuccess(success: any) {
    this.success = success.message;
  }

  handleError(error: any) {
    this.error = error.error.error;
  }

  ngOnInit() {

    this.Jarwis.getSurveys().subscribe((response: any) => {
      this.surveys = response['surveys'];
    })

    this.token = localStorage.getItem('token');
    this.admins = this.Token.payload(this.token);
    this.username = this.admins.data.name;

    let myFormData = new FormData();
    myFormData.append('name', this.username);

    this.Jarwis.getImage(myFormData).subscribe((response: any) => {
      this.pic = response['Image'];
    },
      error => this.handleError(error),
    );

    this.Jarwis.getAllReports().subscribe((response: any) => {
      this.reports = response['reports'];
    },
      error => this.handleError(error),
    );

  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    localStorage.clear();
    this.router.navigate(['']);
  }

}
