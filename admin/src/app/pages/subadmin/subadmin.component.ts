import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { JarwisService } from 'src/app/services/jarwis.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-subadmin',
  templateUrl: './subadmin.component.html',
  styleUrls: ['./subadmin.component.css']
})
export class SubadminComponent implements OnInit {

  public form = {

    name: '',
    email: '',
    password: ''
  }

  public loggedIn: boolean = false;
  public token: any;
  public users: any;
  public user: any;
  public pic: any;

  public error: any;
  public success: any;
  public message = null;
  public subadmins: any;
  public userid: any;
  public footer: any;
  public userEmail: any;

  constructor(
    private Jarwis: JarwisService,
    private Token: TokenService,
    private router: Router,
    private Auth: AuthService,
  ) { }

  ngOnInit() {

    this.Auth.authStatus.subscribe((value: boolean) => this.loggedIn = value);
    this.token = localStorage.getItem('token');
    this.user = this.Token.payload(this.token);
    this.user = this.user.data.name;

    //get admin profile image
    let myFormData = new FormData();
    myFormData.append('name', this.user);

    this.Jarwis.getImage(myFormData).subscribe((response: any) => {
      this.pic = response['Image'];
      console.log(this.pic);
    },
      error => this.handleError(error),
    );


    //retrive footer
    // this.Jarwis.getFooter().subscribe((response: any) => {
    //   this.footer = response['design'];
    // })

    this.Jarwis.getSubadmins().subscribe((response: any) => {
      this.subadmins = response['subadmins'];
    })
  }

  deleteUser(id: any) {
    this.Jarwis.deleteSubadmin({ "id": id }).subscribe(
      data => this.handleResponse(data),
    );
  }

  disableUser(id: any) {
    console.log('user disabled');
  }

  Reset(email: string) {
    this.userEmail = email;
  }

  onSubmit() {
    this.Jarwis.addSubadmin(this.form).subscribe(
      success => this.handleSuccess(success),
      error => this.handleError(error)
    );
  }

  onReset() {

    let myFormData = new FormData();
    myFormData.append('email', this.userEmail);
    myFormData.append('password', this.form.password);

    this.Jarwis.resetPassword2(myFormData).subscribe(
      success => this.handleSuccess(success),
      error => this.handleError(error)
    )
  }

  handleResponse(data: any) {
    this.ngOnInit();
  }

  handleSuccess(success: any) {
    this.success = success.message;
  }

  handleError(error: any) {
    this.error = error.error.error;
  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    localStorage.clear();
    this.router.navigate(['']);
  }

}
