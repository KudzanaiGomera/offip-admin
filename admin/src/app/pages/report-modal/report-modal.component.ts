import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { JarwisService } from 'src/app/services/jarwis.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-report-modal',
  templateUrl: './report-modal.component.html',
  styleUrls: ['./report-modal.component.css']
})
export class ReportModalComponent implements OnInit {

  public loggedIn: boolean = false;
  public user: any;
  public survey: any;
  public surveys: any;
  public selectedSurvey: any;
  public error: any;
  public footer: any;
  public searchValue: any;
  public gridApi: any;
  public gridColumnApi: any;
  public columnDefs: any;
  public sortingOrder: any;
  public frameworkComponents: any;

  public users: any;
  public token: any;
  public username = '';
  public profile: any;
  public pic: any;

  data: any[] = [];

  constructor(
    private Jarwis: JarwisService,
    private router: Router,
    private route: ActivatedRoute,
    private Token: TokenService,
    private Auth: AuthService,
  ) { }

  //get survey name from url
  getName() {
    let surveyName = this.route.snapshot.paramMap.get('name');
    return (surveyName);
  };


  //Columns
  Columns() {
    this.columnDefs = [

      {
        headerName: 'QuestionNo',
        field: 'questionNum',
        width: 250,
        sortingOrder: ["asc", "desc"],
        filter: true,
        sortable: true,
      },
      {
        headerName: 'Question',
        field: 'question',
        width: 450,
        sortingOrder: ["asc", "desc"],
        filter: true,
        sortable: true,
      },
      {
        headerName: 'answer',
        field: 'choice',
        width: 450,
        sortingOrder: ["asc", "desc"],
        filter: true,
        resizable: true,
        sortable: true,
      }
    ];

    this.sortingOrder = ["desc", "asc", null];

  }

  ngOnInit() {
    //display columns for api data
    this.Columns();
    this.survey = this.getName();
    this.Jarwis.getSurveyBySurvey({ 'survey': this.survey }).subscribe((response: any) => {
      this.surveys = response['survey'];
    })

    this.Auth.authStatus.subscribe((value: boolean) => this.loggedIn = value);
    this.token = localStorage.getItem('token');
    this.user = this.Token.payload(this.token);
    this.user = this.user.data.name;

    //get admin profile image
    let myFormData = new FormData();
    myFormData.append('name', this.user);

    this.Jarwis.getImage(myFormData).subscribe((response: any) => {
      this.pic = response['Image'];
      console.log(this.pic);
    },
      error => this.handleError(error),
    );

  }

  //get client Data on click
  getClientData(name: string) {
    let myFormData = new FormData();
    myFormData.append('name', name);

    this.Jarwis.getAccount(myFormData).subscribe((response: any) => {
      this.profile = response['user'];
    },
      error => this.handleError(error)
    );
  }

  //pull data from api and populate the grid
  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.selectedSurvey = this.getName();
    this.Jarwis.getSurveyReport({ 'survey': this.selectedSurvey }).subscribe((response: any) => {
      this.data = response['survey'];
      params.api.setRowData(this.data);
    })
  }

  //function to export reports
  onBtExport() {
    var params = {

    };
    // export to csv
    this.gridApi.exportDataAsCsv(params)
  }

  // function for a filtering all columns at once
  quickSearch() {
    this.gridApi.setQuickFilter(this.searchValue);
  }

  handleError(error: any) {
    this.error = error.error.error;
  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    localStorage.clear();
    this.router.navigate(['']);
  }


}
